/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author Ronald Cuenca
 */
public class persona {
    private Integer IDPersona;
    private String nombre;
    private Integer edad;

    public Integer getIDPersona() {
        return IDPersona;
    }

    public void setIDPersona(Integer IDPersona) {
        this.IDPersona = IDPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }
}
